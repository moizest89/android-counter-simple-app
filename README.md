# Counter Sample


This repository contains an Android Application with a challenge to demonstrate part of my knowledge like Android Developer.

## Challenge Description

Create an Android app for counting things. You'll need to meet high expectations for quality and functionality.


## App Description
![api_url](https://i.imgur.com/hw9WUzw.png)


   
## Installation

1. Clone the repository
2. Open the project in Android Studio
3. Go to file named local.properties and create a variable called `API_URL` like the following example:
![api_url](https://i.imgur.com/UwG5fzG.png)
4. Set the API_URL of the API service `https://counter-sample-app.herokuapp.com/api/v1`
5. Menu option build -> Clean Project. This step creates `BuildConfig.API_URL` value to use in the application.
6. Run the project and start to use it.

### Notes
1. If you use a local server, avoid using `http://localhost:3000`. It's better to replace it with `127.0.0.1`. Application has problems using localhost. 


### Regards
