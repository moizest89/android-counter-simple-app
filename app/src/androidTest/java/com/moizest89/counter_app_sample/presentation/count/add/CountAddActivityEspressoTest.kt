package com.moizest89.counter_app_sample.presentation.count.add

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.MediumTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4

import com.moizest89.counter_app_sample.R
import org.hamcrest.core.IsNot.not
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class CountAddActivityEspressoTest {

    @get:Rule
    val mActivityRule: ActivityTestRule<CountAddActivity> =
        ActivityTestRule(CountAddActivity::class.java)

    @Test
    fun ensureTextChangesWork() {
        //Type simple text
        onView(withId(R.id.default_edit_text)).perform(typeText("Coffee"), closeSoftKeyboard())
        onView(withId(R.id.default_edit_text)).check(matches(withText("Coffee")))
    }

    @Test
    fun ensureButtonActionChangeStatus() {
        val button = withId(R.id.materialButtonSave)
        val editText = withId(R.id.default_edit_text)
        onView(button).check(matches(not(isEnabled())))
        onView(editText).perform(typeText("Test"), closeSoftKeyboard())
        onView(button).check(matches(isEnabled()))
        onView(editText).perform(replaceText(""), closeSoftKeyboard())
        onView(button).check(matches(not(isEnabled())))
        onView(editText).perform(replaceText("  "), closeSoftKeyboard())
        onView(button).check(matches(not(isEnabled())))
    }

    @Test
    fun ensureButtonClickAction() {

        val button = withId(R.id.materialButtonSave)
        val editText = withId(R.id.default_edit_text)
        val progressBar = withId(R.id.progressBar)

        //Check init status
        onView(button).check(matches(isDisplayed()))
        onView(button).check(matches(not(isEnabled())))
        onView(progressBar).check(matches(not(isDisplayed())))

        //Change values
        onView(editText).perform(typeText("Coffee"), closeSoftKeyboard())
        onView(button).check(matches(isDisplayed()))
        onView(button).check(matches(isEnabled()))
        onView(progressBar).check(matches(not(isDisplayed())))

        //Call Action
        onView(editText).perform(click())
        onView(editText).check(matches(withText("Coffee")))
    }

}