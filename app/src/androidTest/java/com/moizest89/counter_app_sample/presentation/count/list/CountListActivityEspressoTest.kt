package com.moizest89.counter_app_sample.presentation.count.list

import android.view.View
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.moizest89.counter_app_sample.R
import com.moizest89.counter_app_sample.presentation.count.add.CountAddActivity
import com.moizest89.counter_app_sample.presentation.utils.NetworkUtils
import com.moizest89.counter_app_sample.presentation.utils.SwipeRefreshLayoutMatchers.isRefreshing
import org.hamcrest.Matcher
import org.hamcrest.core.IsNot.not
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class CountListActivityEspressoTest {

    private lateinit var networkUtils: NetworkUtils
    private lateinit var extended_fab: Matcher<View>

    private lateinit var editTextSearchInformation: Matcher<View>
    private lateinit var imageViewRemoveFocus: Matcher<View>
    private lateinit var cardViewSearchCounter: Matcher<View>

    private lateinit var progressBar: Matcher<View>
    private lateinit var textViewSearchResult: Matcher<View>
    private lateinit var linearLayoutEmptyState: Matcher<View>
    private lateinit var linearLayoutInternetState: Matcher<View>
    private lateinit var materialButtonRetry: Matcher<View>
    private lateinit var swipeRefreshLayout: Matcher<View>

    private lateinit var textViewTotalItems: Matcher<View>
    private lateinit var textViewTotalTimes: Matcher<View>
    private lateinit var recyclerViewData: Matcher<View>

    @get:Rule
    val mActivityRule: ActivityTestRule<CountListActivity> =
        ActivityTestRule(CountListActivity::class.java)

    @Before
    fun setup() {
        Intents.init()
        this.networkUtils = NetworkUtils(mActivityRule.activity)
        this.textViewSearchResult = withId(R.id.textViewSearchResult)
        this.extended_fab = withId(R.id.extended_fab)
        this.cardViewSearchCounter = withId(R.id.editTextSearchInformation)
        this.editTextSearchInformation = withId(R.id.editTextSearchInformation)
        this.imageViewRemoveFocus = withId(R.id.imageViewRemoveFocus)
        this.progressBar = withId(R.id.progressBar)
        this.linearLayoutInternetState = withId(R.id.linearLayoutInternetState)
        this.linearLayoutEmptyState = withId(R.id.linearLayoutEmptyState)
        this.materialButtonRetry = withId(R.id.materialButtonRetry)
        this.swipeRefreshLayout = withId(R.id.swipeRefreshLayout)
        this.textViewTotalItems = withId(R.id.textViewTotalItems)
        this.textViewTotalTimes = withId(R.id.textViewTotalTimes)
        this.recyclerViewData = withId(R.id.recyclerViewData)
    }

    @After
    fun cleanUp() {
        Intents.release()
    }

    @Test
    fun ensureInitViewStatus() {
        onView(this.textViewSearchResult).check(matches(not(isDisplayed())))
    }

    @Test
    fun internetConnectionWithErrors() {
        if (!networkUtils.hasInternetConnection()) {
            onView(this.progressBar).check(matches(not(isDisplayed())))
            onView(this.linearLayoutInternetState).check(matches(not(isDisplayed())))
            onView(this.extended_fab).check(matches(isDisplayed()))
            onView(this.linearLayoutEmptyState).check(matches(not(isDisplayed())))
            onView(this.cardViewSearchCounter).check(matches(not(isDisplayed())))
            onView(this.swipeRefreshLayout).check(matches(not(isDisplayed())))
            onView(this.swipeRefreshLayout).check(matches(not(isRefreshing())))
        }
    }

    @Test
    fun ensureFabLoadNewAddCounterItemView() {
        onView(this.extended_fab).perform(click())
        intended(hasComponent(CountAddActivity::class.java.name))
    }

}