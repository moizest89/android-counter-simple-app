package com.moizest89.counter_app_sample

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CounterApp : Application(){
}