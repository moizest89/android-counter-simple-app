package com.moizest89.counter_app_sample.di

import android.content.Context
import androidx.room.Room
import com.moizest89.counter_app_sample.BuildConfig
import com.moizest89.counter_app_sample.data.datasources.db.CounterDao
import com.moizest89.counter_app_sample.data.datasources.db.CounterDataBase
import com.moizest89.counter_app_sample.data.datasources.db.DataBaseDataSource
import com.moizest89.counter_app_sample.data.datasources.db.DataBaseDataSourceImpl
import com.moizest89.counter_app_sample.data.datasources.remote.ConnectivityInterceptor
import com.moizest89.counter_app_sample.data.datasources.remote.RemoteDataService
import com.moizest89.counter_app_sample.data.datasources.remote.RemoteDataSource
import com.moizest89.counter_app_sample.data.datasources.remote.RemoteDataSourceImpl
import com.moizest89.counter_app_sample.data.datasources.sharepreferences.SharePreference
import com.moizest89.counter_app_sample.data.repository.count.CountRepositoryImpl
import com.moizest89.counter_app_sample.data.repository.user.UserRepositoryImpl
import com.moizest89.counter_app_sample.data.utils.WifiService
import com.moizest89.counter_app_sample.domain.repository.count.CountRepository
import com.moizest89.counter_app_sample.domain.repository.user.UserRepository
import com.moizest89.counter_app_sample.domain.usecases.count.AddCountItemUseCase
import com.moizest89.counter_app_sample.presentation.count.add.CountAddContractor
import com.moizest89.counter_app_sample.presentation.count.add.CountAddPresenter
import com.moizest89.counter_app_sample.presentation.utils.NetworkUtils
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @Singleton
    @Provides
    fun provideHttpClient(
        connectivityInterceptor: ConnectivityInterceptor
    ): OkHttpClient {
        return OkHttpClient
            .Builder()
            .addInterceptor(connectivityInterceptor)
            .readTimeout(15, TimeUnit.SECONDS)
            .connectTimeout(15, TimeUnit.SECONDS)
            .build()
    }

    @Singleton
    @Provides
    fun provideConverterFactory(): GsonConverterFactory =
        GsonConverterFactory.create()

    @Singleton
    @Provides
    fun provideRetrofit(
        okHttpClient: OkHttpClient,
        gsonConverterFactory: GsonConverterFactory
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.API_URL)
            .client(okHttpClient)
            .addConverterFactory(gsonConverterFactory)
            .build()
    }

    @Provides
    @Singleton
    fun provideCounterDataBase(@ApplicationContext appContext: Context): CounterDataBase {
        return Room.databaseBuilder(
            appContext,
            CounterDataBase::class.java,
            "cornershop.counterstest"
        ).build()
    }

    @Provides
    fun provideCounterDao(counterDataBase: CounterDataBase): CounterDao {
        return counterDataBase.counterDao()
    }

    @Singleton
    @Provides
    fun provideCurrencyService(retrofit: Retrofit): RemoteDataService =
        retrofit.create(RemoteDataService::class.java)

    @Singleton
    @Provides
    fun provideNetworkUtils(@ApplicationContext context: Context): NetworkUtils =
        NetworkUtils(context)

    @Singleton
    @Provides
    fun provideWifiService(@ApplicationContext context: Context): WifiService =
        WifiService(context)

    @Provides
    fun providerInterceptor(wifiService: WifiService) = ConnectivityInterceptor(wifiService)

    @Provides
    fun providerSharePreference(@ApplicationContext context: Context) = SharePreference(context)

    //Repositories
    //Counter
    @Provides
    fun provideRemoteDataSourceImpl(
        remoteDataService: RemoteDataService
    ): RemoteDataSource {
        return RemoteDataSourceImpl(remoteDataService)
    }

    @Provides
    fun provideDataBaseDataSourceImpl(
        counterDao: CounterDao
    ): DataBaseDataSource {
        return DataBaseDataSourceImpl(counterDao)
    }

    @Provides
    fun provideCounterRepositoryImpl(
        remoteDataSource: RemoteDataSource,
        dataBaseDataSource: DataBaseDataSource
    ): CountRepository {
        return CountRepositoryImpl(remoteDataSource, dataBaseDataSource)
    }

    //User
    @Provides
    fun provideUserRepositoryImpl(
        sharePreference: SharePreference
    ): UserRepository = UserRepositoryImpl(sharePreference)

}