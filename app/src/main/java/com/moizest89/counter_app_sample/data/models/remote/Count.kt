package com.moizest89.counter_app_sample.data.models.remote

import com.google.gson.annotations.SerializedName

data class CounterResponseItem(
    @SerializedName("count")
    var count: Int = 0,
    @SerializedName("id")
    var id: String = "",
    @SerializedName("title")
    var title: String = ""
)

data class CounterRequestItem(
    @SerializedName("title")
    var title: String? = ""
)

data class CounterActionRequestItem(
    @SerializedName("id")
    var id: String = ""
)