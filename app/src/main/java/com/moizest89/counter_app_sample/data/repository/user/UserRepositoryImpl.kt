package com.moizest89.counter_app_sample.data.repository.user

import com.moizest89.counter_app_sample.data.datasources.sharepreferences.SharePreference
import com.moizest89.counter_app_sample.data.mappers.toUser
import com.moizest89.counter_app_sample.data.mappers.toUserModel
import com.moizest89.counter_app_sample.domain.models.UserModel
import com.moizest89.counter_app_sample.domain.repository.user.UserRepository

class UserRepositoryImpl constructor(
    private val sharePreference: SharePreference,
) : UserRepository {

    override suspend fun getUserInformation() = sharePreference.getUserInformation().toUserModel()
    override suspend fun setUserInformation(userInformation: UserModel) =
        sharePreference.setUserInformation(userInformation.toUser()).toUserModel()
}