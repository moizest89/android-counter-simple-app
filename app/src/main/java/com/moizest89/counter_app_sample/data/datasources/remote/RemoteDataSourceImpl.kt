package com.moizest89.counter_app_sample.data.datasources.remote

import com.moizest89.counter_app_sample.data.models.remote.CounterActionRequestItem
import com.moizest89.counter_app_sample.data.models.remote.CounterRequestItem
import com.moizest89.counter_app_sample.data.models.remote.CounterResponseItem
import retrofit2.Response
import javax.inject.Inject

class RemoteDataSourceImpl @Inject constructor (private var remoteDataService: RemoteDataService) : RemoteDataSource {
    override suspend fun getCounters(): Response<MutableList<CounterResponseItem>> =
        remoteDataService.getCounters()

    override suspend fun addCounterItem(counterRequestItem: CounterRequestItem): Response<MutableList<CounterResponseItem>> =
        remoteDataService.addCounter(counterRequestItem)

    override suspend fun deleteCounter(counterActionRequestItem: CounterActionRequestItem): Response<MutableList<CounterResponseItem>> =
        remoteDataService.deleteCounter(counterActionRequestItem)

    override suspend fun incrementCounter(counterActionRequestItem: CounterActionRequestItem): Response<MutableList<CounterResponseItem>> =
        remoteDataService.incrementCounter(counterActionRequestItem)

    override suspend fun decrementCounter(counterActionRequestItem: CounterActionRequestItem): Response<MutableList<CounterResponseItem>> =
        remoteDataService.decrementCounter(counterActionRequestItem)
}