package com.moizest89.counter_app_sample.data.datasources.db

import com.moizest89.counter_app_sample.data.models.db.CountItem

interface DataBaseDataSource {
    suspend fun insertAllCountItems(countItems: MutableList<CountItem>): MutableList<CountItem>
    suspend fun getAllCountItems(): MutableList<CountItem>
    suspend fun getAllCountItemsByName(name: String): MutableList<CountItem>
    suspend fun deleteAllCountItems()
}