package com.moizest89.counter_app_sample.data.datasources.remote

import com.moizest89.counter_app_sample.data.models.remote.CounterActionRequestItem
import com.moizest89.counter_app_sample.data.models.remote.CounterRequestItem
import com.moizest89.counter_app_sample.data.models.remote.CounterResponseItem
import retrofit2.Response

interface RemoteDataSource {
    suspend fun getCounters(): Response<MutableList<CounterResponseItem>>
    suspend fun addCounterItem(counterRequestItem: CounterRequestItem): Response<MutableList<CounterResponseItem>>
    suspend fun deleteCounter(counterActionRequestItem: CounterActionRequestItem): Response<MutableList<CounterResponseItem>>
    suspend fun incrementCounter(counterActionRequestItem: CounterActionRequestItem): Response<MutableList<CounterResponseItem>>
    suspend fun decrementCounter(counterActionRequestItem: CounterActionRequestItem): Response<MutableList<CounterResponseItem>>
}