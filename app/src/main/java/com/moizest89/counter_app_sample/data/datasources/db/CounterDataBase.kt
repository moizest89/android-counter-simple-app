package com.moizest89.counter_app_sample.data.datasources.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.moizest89.counter_app_sample.data.models.db.CountItem

@Database(entities = [CountItem::class], version = 1)
abstract class CounterDataBase : RoomDatabase() {
    abstract fun counterDao(): CounterDao
}