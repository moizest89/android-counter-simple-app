package com.moizest89.counter_app_sample.data.datasources.remote

import com.moizest89.counter_app_sample.data.models.remote.CounterActionRequestItem
import com.moizest89.counter_app_sample.data.models.remote.CounterRequestItem
import com.moizest89.counter_app_sample.data.models.remote.CounterResponseItem
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.HTTP
import retrofit2.http.POST

interface RemoteDataService {
    @GET("counters")
    suspend fun getCounters(): Response<MutableList<CounterResponseItem>>

    @POST("counter")
    suspend fun addCounter(@Body counterRequestItem: CounterRequestItem): Response<MutableList<CounterResponseItem>>

    @HTTP(method = "DELETE", path = "counter", hasBody = true)
    suspend fun deleteCounter(@Body counterActionRequestItem: CounterActionRequestItem): Response<MutableList<CounterResponseItem>>

    @POST("counter/inc")
    suspend fun incrementCounter(@Body counterActionRequestItem: CounterActionRequestItem): Response<MutableList<CounterResponseItem>>

    @POST("counter/dec")
    suspend fun decrementCounter(@Body counterActionRequestItem: CounterActionRequestItem): Response<MutableList<CounterResponseItem>>
}