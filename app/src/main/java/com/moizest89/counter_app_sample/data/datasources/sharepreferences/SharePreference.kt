package com.moizest89.counter_app_sample.data.datasources.sharepreferences

import android.content.Context
import android.content.SharedPreferences
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import com.google.gson.Gson
import com.moizest89.counter_app_sample.data.models.db.User
import javax.inject.Inject

private const val USER = "user_preferences"

class SharePreference @Inject constructor(private val context: Context) {

    private var encryptedPrefs: SharedPreferences = EncryptedSharedPreferences.create(
        context,
        "counter.app.sample",
        MasterKey.Builder(context)
            .setKeyScheme(MasterKey.KeyScheme.AES256_GCM)
            .build(),
        EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
        EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
    )

    fun get(): SharedPreferences = encryptedPrefs

    fun getUserInformation(): User {
        this.get().getString(USER, "")?.let {
            return if (it.isNotBlank()) Gson().fromJson(it, User::class.java) else User(true)
        }
        return User(false)
    }

    fun setUserInformation(userInformation: User): User {
        this.get().edit()
            .putString(USER, Gson().toJson(userInformation))
            .apply()
        return userInformation
    }

}