package com.moizest89.counter_app_sample.data.mappers

import com.moizest89.counter_app_sample.data.models.db.User
import com.moizest89.counter_app_sample.domain.models.UserModel

fun User.toUserModel() : UserModel =
    UserModel(isFirstTime = this.isFirstTime)

fun UserModel.toUser() : User =
    User(isFirstTime = this.isFirstTime)