package com.moizest89.counter_app_sample.data.repository.count

import com.moizest89.counter_app_sample.data.datasources.db.DataBaseDataSource
import com.moizest89.counter_app_sample.data.datasources.remote.RemoteDataSource
import com.moizest89.counter_app_sample.data.mappers.toCountActionRequest
import com.moizest89.counter_app_sample.data.mappers.toCountItemList
import com.moizest89.counter_app_sample.data.mappers.toCountModel
import com.moizest89.counter_app_sample.data.mappers.toCounterModelList
import com.moizest89.counter_app_sample.data.models.remote.CounterRequestItem
import com.moizest89.counter_app_sample.domain.models.CountModel
import com.moizest89.counter_app_sample.domain.repository.count.CountRepository
import com.moizest89.counter_app_sample.domain.utils.RepositoryResult
import com.moizest89.counter_app_sample.domain.utils.errorMessage
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException

class CountRepositoryImpl constructor(
    private val remoteDataSource: RemoteDataSource,
    private val dataBaseDataSource: DataBaseDataSource
) : CountRepository {

    override suspend fun getAllCounterItems(): Flow<RepositoryResult<MutableList<CountModel>>> {
        return flow {
            //Get data from database
            emit(RepositoryResult.Loading())
            val fromDataBase = dataBaseDataSource.getAllCountItems().toCountModel()
            emit(RepositoryResult.Success(data = fromDataBase))

            //Get data from api service
            emit(RepositoryResult.Loading())
            val fromApiService = fetchCounterItems()
            emit(fromApiService)
            emit(RepositoryResult.Loading(false))
        }
    }

    private suspend fun fetchCounterItems(): RepositoryResult<MutableList<CountModel>> {
        try {
            val response = remoteDataSource.getCounters()
            if (response.isSuccessful) {
                val body = response.body()
                body?.let {
                    this.dataBaseDataSource.insertAllCountItems(body.toCountItemList())
                    return RepositoryResult.Success(body.toCounterModelList())
                }
            }
            return RepositoryResult.Error("${response.code()} ${response.message()}")
        } catch (e: IOException) {
            return e.errorMessage()
        } catch (e: HttpException) {
            return e.errorMessage()
        }
    }

    override suspend fun addCounterItem(name: String): Flow<RepositoryResult<CountModel>> {
        return flow {
            emit(RepositoryResult.Loading())
            emit(fetchAddCounterItem(name))
            emit(RepositoryResult.Loading(false))
        }
    }

    private suspend fun fetchAddCounterItem(name: String): RepositoryResult<CountModel> {
        try {
            val response = remoteDataSource.addCounterItem(CounterRequestItem(name))
            if (response.isSuccessful) {
                val body = response.body()
                body?.let {
                    this.dataBaseDataSource.insertAllCountItems(body.toCountItemList())
                    return RepositoryResult.Success(body.first { it.title == name }
                        .toCountModel())
                }
            }
            return RepositoryResult.Error("${response.code()} ${response.message()}")
        } catch (e: Exception) {
            return e.errorMessage()
        } catch (e: HttpException) {
            return e.errorMessage()
        }
    }

    override suspend fun deleteCounterItem(countModel: CountModel): RepositoryResult<CountModel> {
        try {
            val response = remoteDataSource.deleteCounter(countModel.toCountActionRequest())
            if (response.isSuccessful) {
                val body = response.body()
                body?.let {
                    this.dataBaseDataSource.insertAllCountItems(body.toCountItemList())
                    return RepositoryResult.Success(countModel)
                }
            }
            return RepositoryResult.Error("${response.code()} ${response.message()}")
        } catch (e: Exception) {
            return e.errorMessage()
        } catch (e: HttpException) {
            return e.errorMessage()
        }
    }

    override suspend fun incrementCounterItem(countModel: CountModel): RepositoryResult<CountModel> {
        try {
            val response = remoteDataSource.incrementCounter(countModel.toCountActionRequest())
            if (response.isSuccessful) {
                val body = response.body()
                body?.let {
                    this.dataBaseDataSource.insertAllCountItems(body.toCountItemList())
                    return RepositoryResult.Success(body.first { it.id == countModel.id }
                        .toCountModel())
                }
            }
            return RepositoryResult.Error("${response.code()} ${response.message()}")
        } catch (e: IOException) {
            return e.errorMessage()
        } catch (e: HttpException) {
            return e.errorMessage()
        }
    }

    override suspend fun decrementCounterItem(countModel: CountModel): RepositoryResult<CountModel> {
        try {
            val response = remoteDataSource.decrementCounter(countModel.toCountActionRequest())
            if (response.isSuccessful) {
                val body = response.body()
                body?.let {
                    this.dataBaseDataSource.insertAllCountItems(body.toCountItemList())
                    return RepositoryResult.Success(body.first { it.id == countModel.id }
                        .toCountModel())
                }
            }
            return RepositoryResult.Error("${response.code()} ${response.message()}")
        } catch (e: IOException) {
            return e.errorMessage()
        } catch (e: HttpException) {
            return e.errorMessage()
        }
    }

    override suspend fun searchCountItemsByName(name: String): RepositoryResult<MutableList<CountModel>> {
        return if (name.isBlank()) {
            RepositoryResult.Success(dataBaseDataSource.getAllCountItems().toCountModel())
        } else {
            RepositoryResult.Success(dataBaseDataSource.getAllCountItemsByName(name).toCountModel())
        }
    }
}