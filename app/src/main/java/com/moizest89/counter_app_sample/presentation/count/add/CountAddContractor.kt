package com.moizest89.counter_app_sample.presentation.count.add

import com.moizest89.counter_app_sample.domain.models.CountModel
import com.moizest89.counter_app_sample.domain.utils.Command
import com.moizest89.counter_app_sample.presentation.base.BasePresenter
import com.moizest89.counter_app_sample.presentation.base.BaseView

interface CountAddContractor {
    interface Presenter : BasePresenter {
        fun addNewCounterItem(newItem: String)
    }

    interface View : BaseView<Presenter> {
        fun onShowErrorMessages(commandError: Command.Error)
        fun onLoadingStatus(isLoading: Boolean)
        fun onItemAdded(item: CountModel?)
    }
}