package com.moizest89.counter_app_sample.presentation.count.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.moizest89.counter_app_sample.R
import com.moizest89.counter_app_sample.domain.models.CountModel

class CountListAdapter  (private val onCounterListener: CounterAdapterListener) :
    RecyclerView.Adapter<CountListAdapter.Holder>() {

    private val counterList = mutableListOf<CountModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder =
        Holder(LayoutInflater.from(parent.context).inflate(R.layout.item_count_list, null))

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val itemCounter = counterList[position]
        holder.cardViewMainContainer.setCardBackgroundColor(
            ContextCompat.getColor(
                holder.itemView.context,
                R.color.white
            )
        )
        holder.textViewCounterName.text = itemCounter.title
        holder.textViewCounter.text = itemCounter.count.toString()

        holder.imageViewCheck.visibility = View.GONE
        holder.materialButtonIncrement.visibility = View.VISIBLE
        holder.materialButtonDecrement.visibility = View.VISIBLE
        holder.textViewCounter.visibility = View.VISIBLE

        //Delete
        holder.materialButtonDelete.setOnClickListener {
            this.onCounterListener.onDeleteItemCount(
                itemCounter, position
            )
        }
        //Increment
        holder.materialButtonIncrement.setOnClickListener {
            this.onCounterListener.onIncrementItemCount(
                itemCounter, position
            )
        }
        //Decrement
        val moreThanZero = itemCounter.count > 0
        holder.materialButtonDecrement.isEnabled = moreThanZero
        holder.materialButtonDecrement.alpha = if (moreThanZero) 1.0f else 0.2f
        holder.materialButtonDecrement.setOnClickListener {
            this.onCounterListener.onDecrementItemCount(
                itemCounter, position
            )
        }

        holder.itemView.setOnLongClickListener { v ->
            if (!onLongItemSelected) {
                onCounterListener.onLongItemClick(true, v, itemCounter, position)
                holder.cardViewMainContainer.setCardBackgroundColor(
                    ContextCompat.getColor(
                        holder.itemView.context,
                        R.color.orange_20
                    )
                )
                holder.imageViewCheck.visibility = View.VISIBLE
                holder.materialButtonIncrement.visibility = View.GONE
                holder.materialButtonDecrement.visibility = View.GONE
                holder.textViewCounter.visibility = View.GONE
                onLongItemSelected = true
                onLongItemCounterItemSelected = itemCounter
                onLongItemPositionSelected = position
            } else {
                onCounterListener.onLongItemClick(false, v, itemCounter, position)
                onLongItemSelected = false
                onLongItemCounterItemSelected = null
                onLongItemPositionSelected = -1
            }
            true
        }
        holder.itemView.setOnClickListener { v ->
            if (onLongItemSelected) {
                onCounterListener.onLongItemClick(false, v, itemCounter, position)
                onLongItemSelected = false
                onLongItemCounterItemSelected = null
                onLongItemPositionSelected = -1
            }
        }

    }

    fun addItem(countItem: CountModel?) {
        if (countItem != null) {
            if (this.counterList.isEmpty()) {
                this.addItems(mutableListOf(countItem))
            } else {
                val indexInList = this.counterList.indexOfFirst { it.id == countItem.id }
                if (indexInList != -1) {
                    this.counterList.removeAt(indexInList)
                    this.counterList.add(indexInList, countItem)
                    this.notifyItemChanged(indexInList)
                } else if (indexInList == -1) {
                    this.counterList.add(countItem)
                    this.notifyItemInserted(this.counterList.size - 1)
                }
            }
        }
        this.onTotalSumCounter(this.counterList)
    }

    fun addItems(countItems: MutableList<CountModel>) {
        this.counterList.clear()
        this.counterList.addAll(countItems)
        this.notifyDataSetChanged()
        this.onTotalSumCounter(this.counterList)
    }

    fun deleteItem(countItem: CountModel?) {
        if (countItem != null) {
            val indexInList = this.counterList.indexOfFirst { it.id == countItem.id }
            if (indexInList != -1) {
                this.counterList.removeAt(indexInList)
                this.notifyItemRemoved(indexInList)
            }
        }
        this.onTotalSumCounter(this.counterList)
    }

    fun searchElementInList(predicate: String) {

    }

    fun getLongItemCounterItemSelected(): CountModel? {
        return onLongItemCounterItemSelected
    }

    fun getLongItemPositionSelected(): Int {
        return onLongItemPositionSelected
    }

    private fun onTotalSumCounter(counterList: MutableList<CountModel>) {
        val timesCount = counterList.sumOf { it.count }
        onCounterListener.counterItemsAndTimes(counterList.size, timesCount)
    }

    override fun getItemCount(): Int = this.counterList.size

    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val materialButtonDelete: MaterialButton = itemView.findViewById(R.id.materialButtonDelete)
        val materialButtonIncrement: MaterialButton =
            itemView.findViewById(R.id.materialButtonIncrement)
        val materialButtonDecrement: MaterialButton =
            itemView.findViewById(R.id.materialButtonDecrement)
        val textViewCounterName: TextView = itemView.findViewById(R.id.textViewCounterName)
        val textViewCounter: TextView = itemView.findViewById(R.id.textViewCounter)
        val cardViewMainContainer: CardView = itemView.findViewById(R.id.cardViewMainContainer)
        val imageViewCheck: ImageView = itemView.findViewById(R.id.imageViewCheck)
    }

    interface CounterAdapterListener {
        fun onIncrementItemCount(counterItem: CountModel, position: Int)
        fun onDecrementItemCount(counterItem: CountModel, position: Int)
        fun onDeleteItemCount(counterItem: CountModel?, position: Int)
        fun counterItemsAndTimes(itemsCount: Int, timesCount: Int)
        fun onLongItemClick(
            showActionMode: Boolean,
            view: View,
            counterItem: CountModel?,
            position: Int?
        )
    }

    companion object {
        private var onLongItemSelected = false
        private var onLongItemPositionSelected = -1
        private var onLongItemCounterItemSelected: CountModel? = null
    }
}