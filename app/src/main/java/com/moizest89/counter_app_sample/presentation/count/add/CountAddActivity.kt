package com.moizest89.counter_app_sample.presentation.count.add

import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputLayout
import com.moizest89.counter_app_sample.R
import com.moizest89.counter_app_sample.domain.models.CountModel
import com.moizest89.counter_app_sample.domain.usecases.count.AddCountItemUseCase
import com.moizest89.counter_app_sample.domain.utils.Command
import com.moizest89.counter_app_sample.domain.utils.CommandError
import com.moizest89.counter_app_sample.presentation.base.BaseActivity
import com.moizest89.counter_app_sample.presentation.utils.Utils
import com.moizest89.counter_app_sample.presentation.utils.onTextChange
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class CountAddActivity : BaseActivity() , CountAddContractor.View{

    private lateinit var toolbar: Toolbar
    private lateinit var progressBar: ProgressBar
    private lateinit var materialButtonSave: MaterialButton
    private lateinit var textInputLayoutCounterName: TextInputLayout
    internal lateinit var presenter: CountAddContractor.Presenter
    @Inject lateinit var addOrUpdateUseCase: AddCountItemUseCase


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_count_add)
        this.inflateViews()
        this.createToolbar()


        this.textInputLayoutCounterName.editText?.onTextChange {
            if (it.isNullOrBlank()) {
                materialButtonSave.isEnabled = false
                materialButtonSave.alpha = 0.3f
            } else {
                materialButtonSave.isEnabled = true
                materialButtonSave.alpha = 1f
            }
        }

        this.textInputLayoutCounterName.editText?.setOnEditorActionListener { v, actionId, event ->
            if (actionId != 0 || event.action === KeyEvent.ACTION_DOWN) {
                sendData()
                true
            } else {
                false
            }
        }

        this.materialButtonSave.setOnClickListener {
            sendData()
        }

        this.setPresenter(CountAddPresenter(this, addOrUpdateUseCase))

    }

    private fun sendData() {
        val name = textInputLayoutCounterName.editText?.text.toString().trim()
        if (name.isNotBlank()) {
            this.presenter.addNewCounterItem(textInputLayoutCounterName.editText?.text.toString().trim())
        }
    }

    private fun inflateViews() {
        this.toolbar = this.findViewById(R.id.toolbar)
        this.progressBar = this.findViewById(R.id.progressBar)
        this.materialButtonSave = this.findViewById(R.id.materialButtonSave)
        this.textInputLayoutCounterName = this.findViewById(R.id.textInputLayoutCounterName)
    }

    private fun createToolbar() {
        toolbar.navigationIcon = ContextCompat.getDrawable(this, R.drawable.ic_close)
        toolbar.title = getString(R.string.create_counter)
        setSupportActionBar(toolbar)
    }

    override fun onLoadingStatus(isLoading: Boolean) {
        this.materialButtonSave.visibility =
            if (isLoading) View.GONE else View.VISIBLE
        this.progressBar.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    override fun onItemAdded(item: CountModel?) {
        val bundle = Bundle()
        bundle.putParcelable(Utils.DATA, item)
        val intent = Intent()
        intent.putExtras(bundle)
        setResult(RESULT_OK, intent)
        finish()
    }

    override fun onShowErrorMessages(commandError: Command.Error) {
        when (commandError.error) {
            CommandError.InternetConnection -> {
                Utils.showSimpleErrorDialog(
                    this@CountAddActivity,
                    getString(R.string.error_creating_counter_title),
                    getString(R.string.connection_error_description)
                )
            }
            is CommandError.SimpleErrorMessage -> {
                Utils.showSimpleErrorDialog(
                    this@CountAddActivity,
                    getString(R.string.generic_error_description),
                    commandError.error.message
                )
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun setPresenter(presenter: CountAddContractor.Presenter) {
        this.presenter = presenter
    }

    override fun onDestroy() {
        this.presenter.onDestroy()
        super.onDestroy()
    }
}