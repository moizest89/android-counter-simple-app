package com.moizest89.counter_app_sample.presentation.base

interface BaseView<T> {
    fun setPresenter(presenter: T)
}