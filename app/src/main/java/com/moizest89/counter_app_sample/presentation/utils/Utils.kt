package com.moizest89.counter_app_sample.presentation.utils

import android.content.Context
import androidx.appcompat.app.AlertDialog
import com.moizest89.counter_app_sample.R

object Utils {

    const val DATA = "sendData"

    fun showSimpleErrorDialog(
        context: Context,
        title: String,
        message: String?,
        onPositiveAction: (() -> Unit)? = null
    ) {
        AlertDialog.Builder(context)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(R.string.ok) { _, _ ->
                onPositiveAction?.invoke()
            }
            .setCancelable(false)
            .show()
    }

}