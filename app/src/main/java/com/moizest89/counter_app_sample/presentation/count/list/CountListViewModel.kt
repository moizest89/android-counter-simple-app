package com.moizest89.counter_app_sample.presentation.count.list

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.moizest89.counter_app_sample.domain.models.CountModel
import com.moizest89.counter_app_sample.domain.usecases.count.*
import com.moizest89.counter_app_sample.domain.utils.Command
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import kotlinx.coroutines.flow.collect

@HiltViewModel
class CountListViewModel @Inject constructor(
    private val getAllCounterItemsUseCase: GetAllCounterItemsUseCase,
    private val addCountItemUseCase: AddCountItemUseCase,
    private val deleteCounterItemUseCase: DeleteCounterItemUseCase,
    private val incrementCountByItemUseCase: IncrementCountByItemUseCase,
    private val decrementCountByItemUseCase: DecrementCountByItemUseCase,
    private val searchCountItemByTitleUseCase: SearchCountItemByTitleUseCase
) : ViewModel() {

    private val _counterData: MutableLiveData<Command> = MutableLiveData()
    val counterData: MutableLiveData<Command> = _counterData

    fun addNewCounterItem(name: String) {
        viewModelScope.launch {
            addCountItemUseCase.invoke(name).collect {
                counterData.value = it
            }
        }
    }

    fun getAllCounterItems() {
        viewModelScope.launch {
            counterData.value = Command.Loading(isLoading = true)
            getAllCounterItemsUseCase.invoke().collect {
                counterData.value = it
            }
            counterData.value = Command.Loading(isLoading = false)
        }
    }

    fun incrementCounterItem(countModel: CountModel, position: Int) {
        viewModelScope.launch {
            counterData.value = Command.Loading(isLoading = true)
            counterData.value = incrementCountByItemUseCase.invoke(countModel)
            counterData.value = Command.Loading(isLoading = false)
        }
    }

    fun decrementCounterItem(countModel: CountModel, position: Int) {
        if (countModel.count > 0) {
            viewModelScope.launch {
                counterData.value = Command.Loading(isLoading = true)
                counterData.value = decrementCountByItemUseCase.invoke(countModel)
                counterData.value = Command.Loading(isLoading = false)
            }
        }
    }

    fun searchCounterItemByName(name: String) {
        viewModelScope.launch {
            val items = withContext(Dispatchers.IO) {
                searchCountItemByTitleUseCase.invoke(name)
            }
            counterData.value = items
        }
    }

    fun deleteItemCounterItem(countModel: CountModel, position: Int) {
        viewModelScope.launch {
            counterData.value = Command.Loading(isLoading = true)
            counterData.value = deleteCounterItemUseCase.invoke(countModel)
            counterData.value = Command.Loading(isLoading = false)
        }
    }

}