package com.moizest89.counter_app_sample.presentation.count.add

import com.moizest89.counter_app_sample.domain.usecases.count.AddCountItemUseCase
import com.moizest89.counter_app_sample.domain.utils.Command
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject


class CountAddPresenter @Inject constructor(
    private var view: CountAddContractor.View?,
    private var addOrUpdateUseCase: AddCountItemUseCase
) : CountAddContractor.Presenter {

    override fun addNewCounterItem(newItem: String) {
        CoroutineScope(Dispatchers.IO).launch {
            addOrUpdateUseCase.invoke(newItem).collect {
                processDataInformation(it)
            }
        }
    }

    private fun processDataInformation(data: Command) {
        CoroutineScope(Dispatchers.Main).launch {
            when (data) {
                is Command.AddOrUpdateCountItemData -> {
                    view?.onItemAdded(data.item)
                }
                is Command.Error -> {
                    view?.onShowErrorMessages(data)
                }
                is Command.Loading -> {
                    view?.onLoadingStatus(data.isLoading)
                }
                else -> {
                }
            }
        }
    }


    override fun onDestroy() {
        this.view = null
    }


}