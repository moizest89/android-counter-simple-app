package com.moizest89.counter_app_sample.presentation.base

interface BasePresenter {
    fun onDestroy()
}