package com.moizest89.counter_app_sample.domain.repository.count

import com.moizest89.counter_app_sample.domain.models.CountModel
import com.moizest89.counter_app_sample.domain.utils.RepositoryResult
import kotlinx.coroutines.flow.Flow

interface CountRepository {
    suspend fun getAllCounterItems(): Flow<RepositoryResult<MutableList<CountModel>>>
    suspend fun addCounterItem(name: String): Flow<RepositoryResult<CountModel>>
    suspend fun deleteCounterItem(countModel: CountModel): RepositoryResult<CountModel>
    suspend fun incrementCounterItem(countModel: CountModel): RepositoryResult<CountModel>
    suspend fun decrementCounterItem(countModel: CountModel): RepositoryResult<CountModel>
    suspend fun searchCountItemsByName(name: String): RepositoryResult<MutableList<CountModel>>
}