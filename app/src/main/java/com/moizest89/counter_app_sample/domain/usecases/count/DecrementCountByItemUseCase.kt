package com.moizest89.counter_app_sample.domain.usecases.count

import com.moizest89.counter_app_sample.domain.models.CountModel
import com.moizest89.counter_app_sample.domain.repository.count.CountRepository
import com.moizest89.counter_app_sample.domain.utils.Command
import com.moizest89.counter_app_sample.domain.utils.RepositoryResult
import com.moizest89.counter_app_sample.domain.utils.toCommandError
import javax.inject.Inject

class DecrementCountByItemUseCase @Inject constructor(
    private val counterRepository: CountRepository
)  : CounterActionUseCase  {

    override suspend fun invoke(countModel: CountModel): Command {
        return when(val result = counterRepository.decrementCounterItem(countModel)){
            is RepositoryResult.Success -> Command.AddOrUpdateCountItemData(item = result.data)
            is RepositoryResult.Error -> result.toCommandError()
            is RepositoryResult.Loading -> Command.Loading(result.isLoading)
        }
    }
}