package com.moizest89.counter_app_sample.domain.usecases.count

import com.moizest89.counter_app_sample.domain.repository.count.CountRepository
import com.moizest89.counter_app_sample.domain.utils.Command
import com.moizest89.counter_app_sample.domain.utils.RepositoryResult
import com.moizest89.counter_app_sample.domain.utils.toCommandError
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class AddCountItemUseCase @Inject constructor(
    private val counterRepository: CountRepository
) {

    suspend fun invoke(name: String): Flow<Command> = counterRepository.addCounterItem(name).map {
        when (it) {
            is RepositoryResult.Success -> Command.AddOrUpdateCountItemData(item = it.data)
            is RepositoryResult.Error -> it.toCommandError()
            is RepositoryResult.Loading -> Command.Loading(it.isLoading)
        }
    }.flowOn(Dispatchers.IO)

}