package com.moizest89.counter_app_sample.domain.usecases.count

import com.moizest89.counter_app_sample.domain.repository.count.CountRepository
import com.moizest89.counter_app_sample.domain.utils.Command
import com.moizest89.counter_app_sample.domain.utils.RepositoryResult
import com.moizest89.counter_app_sample.domain.utils.toCommandError
import javax.inject.Inject

class SearchCountItemByTitleUseCase @Inject constructor(
    private val counterRepository: CountRepository
) {
    suspend fun invoke( name : String) : Command {
        return when(val result = counterRepository.searchCountItemsByName(name)){
            is RepositoryResult.Success -> Command.SearchCounterItemData(data = result.data!!)
            is RepositoryResult.Error -> result.toCommandError()
            is RepositoryResult.Loading -> Command.Loading(result.isLoading)
        }
    }
}