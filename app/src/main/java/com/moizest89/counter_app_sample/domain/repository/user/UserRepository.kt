package com.moizest89.counter_app_sample.domain.repository.user

import com.moizest89.counter_app_sample.domain.models.UserModel

interface UserRepository {
    suspend fun getUserInformation(): UserModel
    suspend fun setUserInformation(userInformation: UserModel): UserModel
}