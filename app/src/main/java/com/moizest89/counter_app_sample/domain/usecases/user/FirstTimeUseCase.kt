package com.moizest89.counter_app_sample.domain.usecases.user

import com.moizest89.counter_app_sample.domain.models.UserModel
import com.moizest89.counter_app_sample.domain.repository.user.UserRepository
import javax.inject.Inject

class FirstTimeUseCase @Inject constructor(
    private val userRepository: UserRepository
) {
    suspend fun invoke(): UserModel {
        return userRepository.setUserInformation(UserModel(isFirstTime = false))
    }
}