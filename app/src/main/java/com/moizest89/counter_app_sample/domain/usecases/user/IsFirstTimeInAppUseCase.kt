package com.moizest89.counter_app_sample.domain.usecases.user

import com.moizest89.counter_app_sample.domain.repository.user.UserRepository
import javax.inject.Inject

class IsFirstTimeInAppUseCase @Inject constructor(
    private val userRepository: UserRepository
) {
    suspend fun invoke() : Boolean{
        return userRepository.getUserInformation().isFirstTime
    }
}