package com.moizest89.counter_app_sample.domain.usecases.count

import com.moizest89.counter_app_sample.domain.models.CountModel
import com.moizest89.counter_app_sample.domain.utils.Command

interface CounterActionUseCase {
    suspend fun invoke(countModel: CountModel): Command
}