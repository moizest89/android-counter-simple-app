package com.moizest89.counter_app_sample.data.repository.count

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.runner.AndroidJUnit4
import com.moizest89.counter_app_sample.BuildConfig
import com.moizest89.counter_app_sample.data.datasources.DummyData.FAKE_TITLE_1
import com.moizest89.counter_app_sample.data.datasources.db.CounterDao
import com.moizest89.counter_app_sample.data.datasources.db.CounterDataBase
import com.moizest89.counter_app_sample.data.datasources.db.DataBaseDataSource
import com.moizest89.counter_app_sample.data.datasources.db.DataBaseDataSourceImpl
import com.moizest89.counter_app_sample.data.datasources.remote.ConnectivityInterceptor
import com.moizest89.counter_app_sample.data.datasources.remote.RemoteDataService
import com.moizest89.counter_app_sample.data.datasources.remote.RemoteDataSource
import com.moizest89.counter_app_sample.data.datasources.remote.RemoteDataSourceImpl
import com.moizest89.counter_app_sample.data.utils.WifiService
import com.moizest89.counter_app_sample.domain.models.CountModel
import com.moizest89.counter_app_sample.domain.repository.count.CountRepository
import com.moizest89.counter_app_sample.domain.utils.RepositoryResult
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.runBlocking
import okhttp3.OkHttpClient
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


@RunWith(AndroidJUnit4::class)
@Config(sdk = [29])
class CountRepositoryImplTest {

    private lateinit var countRepository: CountRepository

    //Remote
    private lateinit var remoteDataSource: RemoteDataSource
    private lateinit var okHttpClient: OkHttpClient
    private lateinit var connectivityInterceptor: ConnectivityInterceptor
    private lateinit var wifiService: WifiService
    private lateinit var gsonConverterFactory: GsonConverterFactory
    private lateinit var retrofit: Retrofit
    private lateinit var remoteDataService: RemoteDataService

    //Database
    private lateinit var counterDao: CounterDao
    private lateinit var counterDataBase: CounterDataBase
    private lateinit var dataBaseDataSource: DataBaseDataSource

    @Before
    fun setup() {
        //Remote
        val context = ApplicationProvider.getApplicationContext<Context>()
        this.wifiService = WifiService(context)
        this.gsonConverterFactory = GsonConverterFactory.create()
        this.connectivityInterceptor = ConnectivityInterceptor(this.wifiService)
        this.okHttpClient = OkHttpClient
            .Builder()
            .addInterceptor(connectivityInterceptor)
            .readTimeout(15, TimeUnit.SECONDS)
            .connectTimeout(15, TimeUnit.SECONDS)
            .build()
        this.retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.API_URL)
            .client(okHttpClient)
            .addConverterFactory(gsonConverterFactory)
            .build()
        this.remoteDataService = retrofit.create(RemoteDataService::class.java)
        this.remoteDataSource = RemoteDataSourceImpl(this.remoteDataService)

        counterDataBase = Room.inMemoryDatabaseBuilder(
            context, CounterDataBase::class.java
        ).build()
        counterDao = counterDataBase.counterDao()
        this.dataBaseDataSource = DataBaseDataSourceImpl(counterDao)
        this.countRepository = CountRepositoryImpl(this.remoteDataSource, this.dataBaseDataSource)
    }


    @Test
    fun `save basic count item`() {
        runBlocking {
            countRepository.addCounterItem(FAKE_TITLE_1).collect {
            }
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `get all basic items list`() {
        runBlocking {
            val flow = countRepository.getAllCounterItems().take(2)
            flow.collect { result : RepositoryResult<MutableList<CountModel>> ->
//                assertEquals(result.data , mutableListOf<CountModel>())
            }
        }
    }

}