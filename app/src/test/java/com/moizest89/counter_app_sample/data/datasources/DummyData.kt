package com.moizest89.counter_app_sample.data.datasources

import com.moizest89.counter_app_sample.data.models.db.CountItem

object DummyData {

    const val FAKE_ID_1 = "kssf88es"
    const val FAKE_ID_2 = "kssf942a"
    const val FAKE_ID_3 = "kssf94y4"

    const val FAKE_TITLE_1 = "Coffee 1"
    const val FAKE_TITLE_2 = "Coffee 2"
    const val FAKE_TITLE_3 = "Coffee 3"

    const val FAKE_SEARCH_1 = "Coffee"
    const val FAKE_SEARCH_2 = "1"
    const val FAKE_SEARCH_3 = "Cappuccino"
    const val FAKE_SEARCH_4 = "Coffee 2"

    const val FAKE_COUNT_1 = 1
    const val FAKE_COUNT_2 = 2
    const val FAKE_COUNT_3 = 3

    val simpleList = mutableListOf(
        CountItem(id = FAKE_ID_1, count = FAKE_COUNT_1, title = FAKE_TITLE_1),
    )

    val listTwoElements = mutableListOf(
        CountItem(id = FAKE_ID_1, count = FAKE_COUNT_1, title = FAKE_TITLE_1),
        CountItem(id = FAKE_ID_2, count = FAKE_COUNT_2, title = FAKE_TITLE_2)
    )

    val listThreeElements = mutableListOf(
        CountItem(id = FAKE_ID_1, count = FAKE_COUNT_1, title = FAKE_TITLE_1),
        CountItem(id = FAKE_ID_2, count = FAKE_COUNT_2, title = FAKE_TITLE_2),
        CountItem(id = FAKE_ID_3, count = FAKE_COUNT_3, title = FAKE_TITLE_3)
    )
}