package com.moizest89.counter_app_sample.data.repository.user

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.runner.AndroidJUnit4
import com.moizest89.counter_app_sample.FakeAndroidKeyStore
import com.moizest89.counter_app_sample.data.datasources.sharepreferences.SharePreference
import com.moizest89.counter_app_sample.domain.models.UserModel
import com.moizest89.counter_app_sample.domain.repository.user.UserRepository
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

@RunWith(AndroidJUnit4::class)
@Config(sdk = [29])
class UserRepositoryImplTest {

    private lateinit var sharedPreference: SharePreference
    private lateinit var userRepository: UserRepository


    @Before
    fun setup() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        this.sharedPreference = SharePreference(context)
        this.userRepository = UserRepositoryImpl(this.sharedPreference)
    }

    @Test
    fun `get default user information`() {
        runBlocking {
            assertNotNull(userRepository.getUserInformation())
        }
    }

    @Test
    fun `save user when is first time`() {
        runBlocking {
            userRepository.setUserInformation(UserModel(isFirstTime = false))
            val newUser = userRepository.getUserInformation()
            assertEquals(newUser.isFirstTime, false)
        }
    }

    companion object {
        @JvmStatic
        @BeforeClass
        fun beforeClass() {
            FakeAndroidKeyStore.setup
        }
    }

}