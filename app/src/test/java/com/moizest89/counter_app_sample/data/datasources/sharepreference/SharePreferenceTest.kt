package com.moizest89.counter_app_sample.data.datasources.sharepreference

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.runner.AndroidJUnit4
import com.moizest89.counter_app_sample.FakeAndroidKeyStore
import com.moizest89.counter_app_sample.data.datasources.sharepreferences.SharePreference
import com.moizest89.counter_app_sample.data.models.db.User
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

@RunWith(AndroidJUnit4::class)
@Config(sdk = [29])
class SharePreferenceTest {

    private lateinit var sharedPreference: SharePreference

    @Before
    fun setup() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        this.sharedPreference = SharePreference(context)
    }

    @Test
    fun `save user information`() {
        val user = User(isFirstTime = true)
        sharedPreference.setUserInformation(user)
        assertEquals(sharedPreference.getUserInformation(), user)
    }

    companion object {

        @JvmStatic
        @BeforeClass
        fun beforeClass() {
            FakeAndroidKeyStore.setup
        }
    }
}