package com.moizest89.counter_app_sample.data.datasources.remote

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.runner.AndroidJUnit4
import com.moizest89.counter_app_sample.BuildConfig
import com.moizest89.counter_app_sample.data.datasources.DummyData.FAKE_TITLE_1
import com.moizest89.counter_app_sample.data.models.remote.CounterRequestItem
import com.moizest89.counter_app_sample.data.utils.WifiService
import junit.framework.Assert.*
import kotlinx.coroutines.runBlocking
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


@RunWith(AndroidJUnit4::class)
@Config(sdk = [29])
class RemoteDataSourceTest {

    private lateinit var remoteDataSource: RemoteDataSource
    private lateinit var okHttpClient: OkHttpClient
    private lateinit var connectivityInterceptor: ConnectivityInterceptor
    private lateinit var wifiService: WifiService
    private lateinit var gsonConverterFactory: GsonConverterFactory
    private lateinit var retrofit: Retrofit
    private lateinit var remoteDataService: RemoteDataService
    private val mockWebServer = MockWebServer()

    @Before
    fun setup() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        this.wifiService = WifiService(context)
        this.gsonConverterFactory = GsonConverterFactory.create()
        this.connectivityInterceptor = ConnectivityInterceptor(this.wifiService)
        this.okHttpClient = OkHttpClient
            .Builder()
            .addInterceptor(connectivityInterceptor)
            .readTimeout(15, TimeUnit.SECONDS)
            .connectTimeout(15, TimeUnit.SECONDS)
            .build()
        this.retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.API_URL)
            .client(okHttpClient)
            .addConverterFactory(gsonConverterFactory)
            .build()
        this.remoteDataService = retrofit.create(RemoteDataService::class.java)
        this.remoteDataSource = RemoteDataSourceImpl(this.remoteDataService)
    }


    @Test
    fun `test basic request`() {
        runBlocking {
            if(wifiService.isOnline()) {
                val request = remoteDataSource.getCounters()
                assertEquals(request.code(), 200)
            }
        }
    }

    @Test
    fun `test failure request`() {
        runBlocking {
            if(!wifiService.isOnline()) {
                val request = remoteDataSource.getCounters()
                assertEquals(request.code(), 200)
            }
        }
    }

    @Test
    fun `test create new counter item`() {
        runBlocking {
            if(wifiService.isOnline()) {
                val request = remoteDataSource.addCounterItem(CounterRequestItem(FAKE_TITLE_1))
                assertEquals(request.code(), 200)
                assertFalse(request.body()?.isEmpty()!!)
            }
        }
    }

    @After
    fun close() {
        mockWebServer.close()
    }

}