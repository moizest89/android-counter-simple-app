package com.moizest89.counter_app_sample.data.datasources.db

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.runner.AndroidJUnit4
import com.moizest89.counter_app_sample.data.datasources.DummyData.FAKE_SEARCH_1
import com.moizest89.counter_app_sample.data.datasources.DummyData.FAKE_SEARCH_2
import com.moizest89.counter_app_sample.data.datasources.DummyData.FAKE_SEARCH_3
import com.moizest89.counter_app_sample.data.datasources.DummyData.FAKE_TITLE_1
import com.moizest89.counter_app_sample.data.datasources.DummyData.listThreeElements
import com.moizest89.counter_app_sample.data.datasources.DummyData.listTwoElements
import com.moizest89.counter_app_sample.data.datasources.DummyData.simpleList
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import org.junit.After
import org.junit.Assert.assertNotEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config


@RunWith(AndroidJUnit4::class)
@Config(sdk = [29])
class DataBaseTest {

    private lateinit var counterDao: CounterDao
    private lateinit var counterDataBase: CounterDataBase
    private lateinit var dataBaseDataSource: DataBaseDataSource


    @ExperimentalCoroutinesApi
    @Before
    fun createDb() {

        val context = ApplicationProvider.getApplicationContext<Context>()
        counterDataBase = Room.inMemoryDatabaseBuilder(
            context, CounterDataBase::class.java
        ).build()
        counterDao = counterDataBase.counterDao()
        this.dataBaseDataSource = DataBaseDataSourceImpl(counterDao)
    }

    @Test
    fun `Save a simple Count item`() {
        runBlocking {
            dataBaseDataSource.insertAllCountItems(simpleList)
            val countItemRequest = dataBaseDataSource.getAllCountItems()
            assertEquals(1, countItemRequest.size)
        }
    }

    @Test
    fun `Save a list of Count items`() {
        runBlocking {
            dataBaseDataSource.insertAllCountItems(listTwoElements)
            val countItemRequest = dataBaseDataSource.getAllCountItems()
            assertEquals(2, countItemRequest.size)
        }
    }

    @Test
    fun `Save a multiple list of Count items`() {
        runBlocking {
            //First iteration
            dataBaseDataSource.insertAllCountItems(listTwoElements)
            val firstItemRequest = dataBaseDataSource.getAllCountItems()
            assertEquals(2, firstItemRequest.size)
            //Second iteration
            dataBaseDataSource.insertAllCountItems(listThreeElements)
            val secondItemRequest = dataBaseDataSource.getAllCountItems()
            assertNotEquals(5, secondItemRequest.size);
            assertEquals(3, secondItemRequest.size)
        }
    }

    @Test
    fun `Delete list of Count items`() {
        runBlocking {
            //First iteration
            dataBaseDataSource.insertAllCountItems(listTwoElements)
            val firstItemRequest = dataBaseDataSource.getAllCountItems()
            assertEquals(2, firstItemRequest.size)
            //Second iteration
            dataBaseDataSource.deleteAllCountItems()
            val secondItemRequest = dataBaseDataSource.getAllCountItems()
            assertNotEquals(2, secondItemRequest.size);
            assertEquals(0, secondItemRequest.size)
        }
    }

    @Test
    fun `Search exist item in list`() {
        runBlocking {
            //First iteration
            dataBaseDataSource.insertAllCountItems(listThreeElements)
            val searchRequest = dataBaseDataSource.getAllCountItemsByName(FAKE_SEARCH_1)
            assertEquals(3, searchRequest.size)
        }
    }

    @Test
    fun `Search exist specific item in list`() {
        runBlocking {
            //First iteration
            dataBaseDataSource.insertAllCountItems(listThreeElements)
            val searchRequest = dataBaseDataSource.getAllCountItemsByName(FAKE_TITLE_1)
            assertEquals(1, searchRequest.size)
        }
    }

    @Test
    fun `Search exist item in list that contains text`() {
        runBlocking {
            //First iteration
            dataBaseDataSource.insertAllCountItems(listThreeElements)
            val searchRequest = dataBaseDataSource.getAllCountItemsByName(FAKE_SEARCH_2)
            assertEquals(1, searchRequest.size)
        }
    }

    @Test
    fun `Search not exist item in list`() {
        runBlocking {
            //First iteration
            dataBaseDataSource.insertAllCountItems(listThreeElements)
            val searchRequest = dataBaseDataSource.getAllCountItemsByName(FAKE_SEARCH_3)
            assertEquals(0, searchRequest.size)
        }
    }


    @After
    @ExperimentalCoroutinesApi
    fun closeDb() {
        counterDataBase.close()
        Dispatchers.resetMain()
    }

}