package com.moizest89.counter_app_sample.presentation.count.list

import dagger.hilt.android.testing.HiltAndroidTest
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Test


@HiltAndroidTest
//@Config(sdk = { Build.VERSION_CODES.VERSION_CODE}, application = HiltTestApplication.class)
class CountListViewModelTest {

//    @ExperimentalCoroutinesApi
//    private val testDispatcher = TestCoroutineDispatcher()

//    @get:Rule val hiltRule = HiltAndroidRule(this)


    @Before
    fun setup() {
//        hiltRule.inject()
//        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun tearDown() {
//        Dispatchers.resetMain()
//        testDispatcher.cleanupTestCoroutines()
    }

    @ExperimentalCoroutinesApi
    @Test
    fun testsSaveSessionData() = runBlockingTest {
//        val getAllData = counterListViewModel.getAllCounterItems()
        assertEquals("test", "test")
    }

}